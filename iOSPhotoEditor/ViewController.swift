//
//  ViewController.swift
//  iOSPhotoEditor
//
//  Created by Aashish Bogati on 01/03/2021.
//  Copyright © 2021 Ashish Bogati. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ViewController: UIViewController {

    var audioPlayer: AVAudioPlayer!
    var avPlayer: AVPlayer!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func onImageEditor(_ sender: UIButton) {
        
        
        let editor = UIStoryboard.init(name: "Drawing", bundle: nil).instantiateViewController(withIdentifier: "ImageAnnotationViewController") as! ImageAnnotationViewController
        editor.image = UIImage(named: "image")
        self.navigationController?.pushViewController(editor, animated: true)
    }
    
    func playSound(soundUrl: String) {
        let sound = URL(fileURLWithPath: soundUrl)
        do {
            let audioPlayer = try AVAudioPlayer(contentsOf: sound)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        }catch let error {
            print("Error: \(error.localizedDescription)")
        }
    }
    
    @IBAction func onPlayButton(_ sender: UIButton) {
//       let url  = URL.init(string: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3")
        guard let url = URL(string: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3") else {
            return
        }
//       let playerItem: AVPlayerItem = AVPlayerItem(url: url!)
//
//       player = AVPlayer(playerItem: playerItem)
//       player.play()
        self.avPlayer = AVPlayer(playerItem: AVPlayerItem(url: url))
        self.avPlayer.automaticallyWaitsToMinimizeStalling = false
        avPlayer!.volume = 1.0
        avPlayer.play()
    }
}
