//
//  PhotoEditorViewController.swift
//  iOSPhotoEditor
//
//  Created by Aashish Bogati on 01/03/2021.
//  Copyright © 2021 Ashish Bogati. All rights reserved.
//

import UIKit

extension UIImageView {
  func enableZoom() {
    let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(startZooming(_:)))
    isUserInteractionEnabled = true
    addGestureRecognizer(pinchGesture)
  }

  @objc
  private func startZooming(_ sender: UIPinchGestureRecognizer) {
    
  }
}

class PhotoEditorViewController: UIViewController, UIScrollViewDelegate {
    
    // MARK: - Properties
    @IBOutlet weak var canvasView: UIView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var canvasImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    var pin1ViewCenter : CGPoint = .zero
    var pin2ViewCenter : CGPoint = .zero
    
    public var image: UIImage?
    /**
     Array of Stickers -UIImage- that the user will choose from
     */
    public var stickers : [UIImage] = []
    /**
     Array of Colors that will show while drawing or typing
     */
    public var colors  : [UIColor] = []
    //
    //       public var photoEditorDelegate: PhotoEditorDelegate?
    //       var colorsCollectionViewDelegate: ColorsCollectionViewDelegate!
    //
    //       // list of controls to be hidden
    //       public var hiddenControls : [control] = []
    //
    var stickersVCIsVisible = false
    var drawColor: UIColor = UIColor.black
    var textColor: UIColor = UIColor.white
    var isDrawing: Bool = false
    var lastPoint: CGPoint!
    var swiped = false
    var lastPanPoint: CGPoint?
    var lastTextViewTransform: CGAffineTransform?
    var lastTextViewTransCenter: CGPoint?
    var lastTextViewFont:UIFont?
    var activeTextView: UITextView?
    var imageViewToPan: UIImageView?
    var isTyping: Bool = false
    @IBOutlet weak var drawButton: UIButton!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setImageView(image: image!)
        self.scrollView.delegate = self
        self.scrollView.isScrollEnabled = true
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 3.0

        pin1ViewCenter = imageView.center
        pin2ViewCenter = canvasImageView.center

//        self.scrollView.contentSize = self.imageContainerView.frame.size
//        self.canvasImageView.enableZoom()
//        self.imageView.enableZoom()
        
        self.drawButton.addTarget(self, action: #selector(onDrawButton), for: .touchUpInside)
    }
    
    // MARK: - Functions
    func setImageView(image: UIImage) {
        imageView.image = image
        let size = image.suitableSize(widthLimit: UIScreen.main.bounds.width)
        imageViewHeightConstraint.constant = (size?.height)!
    }
    
    override public func touchesBegan(_ touches: Set<UITouch>,
                                      with event: UIEvent?){
        
        if (event?.allTouches!.count)! > 1 {
            self.scrollView.isUserInteractionEnabled = true
        }
        else {
            self.scrollView.isUserInteractionEnabled = false
            swiped = false
            if let touch = touches.first {
                lastPoint = touch.location(in: self.canvasImageView)
            }
        }
        
//        if isDrawing {
//            swiped = false
//            if let touch = touches.first {
//                lastPoint = touch.location(in: self.canvasImageView)
//            }
//        }
        
    }
    
    
    
    override public func touchesMoved(_ touches: Set<UITouch>,
                                      with event: UIEvent?){
        if (event?.allTouches!.count)! > 1 {
            
        }
        else {
            swiped = true
            if let touch = touches.first {
                let currentPoint = touch.location(in: canvasImageView)
                drawLineFrom(lastPoint, toPoint: currentPoint)
                
                // 7
                lastPoint = currentPoint
            }
        }
        
//        if isDrawing {
//            // 6
//            swiped = true
//            if let touch = touches.first {
//                let currentPoint = touch.location(in: canvasImageView)
//                drawLineFrom(lastPoint, toPoint: currentPoint)
//                
//                // 7
//                lastPoint = currentPoint
//            }
//        }
    }
    
    override public func touchesEnded(_ touches: Set<UITouch>,
                                      with event: UIEvent?){
        
        if (event?.allTouches!.count)! > 1 {
//            self.scrollView.isUserInteractionEnabled = false
        }
        else {
            if !swiped {
                // draw a single point
                drawLineFrom(lastPoint, toPoint: lastPoint)
            }
        }
        
        self.scrollView.isUserInteractionEnabled = true
//        if isDrawing {
//            if !swiped {
//                // draw a single point
//                drawLineFrom(lastPoint, toPoint: lastPoint)
//            }
//        }
        
        
    }
    
    
    @objc func onDrawButton() {
        self.isDrawing = true
        self.drawButton.setTitle("Done", for: .normal)
        self.drawButton.removeTarget(self, action: #selector(onDrawButton), for: .touchUpInside)
        self.drawButton.addTarget(self, action: #selector(onDoneButton), for: .touchUpInside)
        self.scrollView.isUserInteractionEnabled = false
    }
    
    @objc func zoomImageView() {
       
    }
    
    @objc func onDoneButton() {
        self.isDrawing = false
        self.drawButton.setTitle("Draw", for: .normal)
        self.drawButton.removeTarget(self, action: #selector(onDoneButton), for: .touchUpInside)
        self.drawButton.addTarget(self, action: #selector(onDrawButton), for: .touchUpInside)
//        self.scrollView.isUserInteractionEnabled = true
    }
    
    
    func drawLineFrom(_ fromPoint: CGPoint, toPoint: CGPoint) {
        // 1
        let canvasSize = canvasImageView.frame.integral.size
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, 0)
        if let context = UIGraphicsGetCurrentContext() {
            canvasImageView.image?.draw(in: CGRect(x: 0, y: 0, width: canvasSize.width, height: canvasSize.height))
            // 2
            context.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
            context.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
            // 3
            context.setLineCap( CGLineCap.round)
            context.setLineWidth(5.0)
            context.setStrokeColor(drawColor.cgColor)
            context.setBlendMode( CGBlendMode.normal)
            // 4
            context.strokePath()
            // 5
            canvasImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
    }
    
    
//    func scrollViewDidZoom(_ scrollView: UIScrollView) {
//        let scaleAffineTransform = CGAffineTransform.identity.scaledBy(x: scrollView.zoomScale, y: scrollView.zoomScale)
//        var translatedPoint = pin1ViewCenter.applying(scaleAffineTransform)
//        imageView.transform = CGAffineTransform.identity.translatedBy(x: translatedPoint.x - pin1ViewCenter.x , y: translatedPoint.y - pin1ViewCenter.y)
//        translatedPoint = pin2ViewCenter.applying(scaleAffineTransform)
//        canvasImageView.transform = CGAffineTransform.identity.translatedBy(x: translatedPoint.x - pin2ViewCenter.x, y: translatedPoint.y - pin2ViewCenter.y)
//    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        let scaleAffineTransform = CGAffineTransform.identity.scaledBy(x: scale, y: scale)
        scrollView.contentSize = self.imageContainerView.bounds.size.applying(scaleAffineTransform)
        self.scrollView.isUserInteractionEnabled = false
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageContainerView
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
//        self.scrollView.isUserInteractionEnabled = true
    }
}

public extension UIImage {
    
    /**
     Suitable size for specific height or width to keep same image ratio
     */
    func suitableSize(heightLimit: CGFloat? = nil,
                      widthLimit: CGFloat? = nil )-> CGSize? {
        
        if let height = heightLimit {
            
            let width = (height / self.size.height) * self.size.width
            
            return CGSize(width: width, height: height)
        }
        
        if let width = widthLimit {
            let height = (width / self.size.width) * self.size.height
            return CGSize(width: width, height: height)
        }
        
        return nil
    }
    
    
}

