////
////  DrawingViewController.swift
////  iOSPhotoEditor
////
////  Created by Aashish Bogati on 03/03/2021.
////  Copyright © 2021 Ashish Bogati. All rights reserved.
////
//
//import UIKit
//
//
//
//class DrawingViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
//
//    // MARK: - Properties
//    public var image: UIImage?
//    var drawColor: UIColor = UIColor.red
//    var isDrawing: Bool = false
//    var lastPoint: CGPoint!
//    var lastPanPoint: CGPoint?
//    var isToolBarButtonSelected: Bool = false
//
//    @IBOutlet weak var canvasView: UIView!
//    @IBOutlet weak var toolsView: UIView!
//    @IBOutlet weak var clearButton: UIButton!
//    @IBOutlet weak var addTextButton: UIButton!
//    @IBOutlet weak var colorSelectorButton: UIButton!
//    @IBOutlet weak var markUpButton: UIButton!
//    @IBOutlet weak var saveButton: UIButton!
//
//    @IBOutlet weak var scrollView: UIScrollView!
//    @IBOutlet weak var deleteView: UIView!
//    @IBOutlet weak var imageContainerView: UIView!
//    @IBOutlet weak var canvasImageView: UIImageView!
//    @IBOutlet weak var imageView: UIImageView!
//    @IBOutlet weak var canvasImageHeight: NSLayoutConstraint!
//
//    var lastTextViewTransform: CGAffineTransform?
//    var lastTextViewTransCenter: CGPoint?
//    var lastTextViewFont:UIFont?
//    var activeTextView: UITextView?
//    var imageViewToPan: UIImageView?
//
//    var pin1ViewCenter : CGPoint = .zero
//    var pin2ViewCenter : CGPoint = .zero
//    var buttonType: ButtonType = .draw
//
//    @IBOutlet weak var colorLIstContainerView: UIView!
//    @IBOutlet weak var collectionView: UICollectionView!
//
//    var colorList: [UIColor] = [.red, .green, .black, .yellow, .orange, .brown]
//    var isTyping: Bool = false
//
//    // MARK: - Life cycle
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Do any additional setup after loading the view.
//        self.configureViews()
//        self.configureNavigationBar()
//        self.configureCollectionView()
//
//    }
//
//    // MARK: - Functions
//    func configureViews() {
//        self.setImageView(image: image!)
//        self.clearButton.layer.cornerRadius = 25
//        self.addTextButton.layer.cornerRadius = 25
//        self.colorSelectorButton.layer.cornerRadius = 25
//        self.markUpButton.layer.cornerRadius = 25
//        self.saveButton.layer.cornerRadius = 25
//
//        self.scrollView.delegate = self
//        self.scrollView.isScrollEnabled = true
//        self.scrollView.minimumZoomScale = 1.0
//        self.scrollView.maximumZoomScale = 3.0
//
////        let twoFingerGesture = UIPinchGestureRecognizer(target: self, action: #selector(onScrollPinchGesture))
////        twoFingerGesture.delegate = self
////        scrollView.addGestureRecognizer(twoFingerGesture)
//////
////        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(onScrollViewPanGesture(_:)))
////        panGesture.delegate = self
////        scrollView.addGestureRecognizer(panGesture)
////
//        pin1ViewCenter = imageView.center
//        pin2ViewCenter = canvasImageView.center
//
//        self.colorLIstContainerView.isHidden = true
//
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow),
//                                               name: UIResponder.keyboardDidShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
//                                               name: UIResponder.keyboardWillHideNotification, object: nil)
//        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillChangeFrame(_:)),
//                                               name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
//
//        self.deleteView.isHidden = true
//
//    }
//
//    func configureNavigationBar() {
//        self.navigationController?.navigationBar.barTintColor = .white
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
//        //        self.navigationController?.navigationItem.setRightBarButton(UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(onDoneButton)), animated: true)
//
//    }
//
//    func addDoneNavigationButton() {
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(onDoneButton))
//    }
//
//    func configureCollectionView() {
//        self.collectionView.delegate = self
//        self.collectionView.dataSource = self
//    }
//
//    func setImageView(image: UIImage) {
//        imageView.image = image
//        let size = image.suitableSize(widthLimit: UIScreen.main.bounds.width)
//        canvasImageHeight.constant = (size?.height)!
//    }
//
//    func drawLineFrom(_ fromPoint: CGPoint, toPoint: CGPoint) {
//        let canvasSize = canvasImageView.frame.integral.size
//        UIGraphicsBeginImageContextWithOptions(canvasSize, false, 0)
//        if let context = UIGraphicsGetCurrentContext() {
//            canvasImageView.image?.draw(in: CGRect(x: 0, y: 0, width: canvasSize.width, height: canvasSize.height))
//
//            context.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
//            context.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
//
//            context.setLineCap( CGLineCap.round)
//            context.setLineWidth(3.0)
//            context.setStrokeColor(drawColor.cgColor)
//            context.setBlendMode( CGBlendMode.normal)
//
//            context.strokePath()
//
//            canvasImageView.image = UIGraphicsGetImageFromCurrentImageContext()
//        }
//        UIGraphicsEndImageContext()
//
//    }
//
//    func addDoneButtonOnKeyboard(view: UITextView){
//        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
//        doneToolbar.barStyle = .default
//
//        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
//
//        let items = [flexSpace, done]
//        doneToolbar.items = items
//        doneToolbar.sizeToFit()
//
//        view.inputAccessoryView = doneToolbar
//    }
//
//
//    func addGestures(view: UIView) {
//        //Gestures
//
//        view.isUserInteractionEnabled = true
//        let panGesture = UIPanGestureRecognizer(target: self,
//                                                action: #selector(DrawingViewController.panGesture))
//        panGesture.minimumNumberOfTouches = 1
//        panGesture.maximumNumberOfTouches = 1
//        panGesture.delegate = self
//        view.addGestureRecognizer(panGesture)
//
//        let pinchGesture = UIPinchGestureRecognizer(target: self,
//                                                    action: #selector(DrawingViewController.pinchGesture))
//        pinchGesture.delegate = self
//        view.addGestureRecognizer(pinchGesture)
//
//        let rotationGestureRecognizer = UIRotationGestureRecognizer(target: self,
//                                                                    action:#selector(DrawingViewController.rotationGesture) )
//        rotationGestureRecognizer.delegate = self
//        view.addGestureRecognizer(rotationGestureRecognizer)
//
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(DrawingViewController.tapGesture))
//        view.addGestureRecognizer(tapGesture)
//
//    }
//
//
//
//    // MARK: - Actions
//    @objc func doneButtonAction(){
//        self.view.endEditing(true)
//        self.isTyping = false
//    }
//
//    @objc func onScrollViewPanGesture(_ recognizer: UIPanGestureRecognizer) {
//        self.enableGestureInScrollView()
//
//        if recognizer.state == .ended {
//            self.disableGestureInScrollView()
//        }
//    }
//
//    @objc func onScrollPinchGesture(_ recognizer: UIPinchGestureRecognizer) {
//        self.enableGestureInScrollView()
//
//        if recognizer.state == .ended {
//            self.disableGestureInScrollView()
//        }
//    }
//
//    @IBAction func onDrawButton(_ sender: UIButton) {
//        self.addDoneNavigationButton()
//        self.isDrawing = true
//        self.buttonType = .draw
//        self.colorLIstContainerView.isHidden = false
//        self.toolsView.isHidden = true
//        self.scrollView.isUserInteractionEnabled = false
//    }
//
//    @objc func onDoneButton(){
//        self.scrollView.isUserInteractionEnabled = true
//        self.colorLIstContainerView.isHidden = true
//        self.navigationItem.rightBarButtonItem = nil
//        self.toolsView.isHidden = false
//    }
//
//    @IBAction func onClearButton(_ sender: UIButton) {
//        canvasImageView.image = nil
//        //clear stickers and textviews
//        for subview in canvasImageView.subviews {
//            subview.removeFromSuperview()
//        }
//    }
//
//
//    @IBAction func onAddTextButton(_ sender: UIButton) {
//        isTyping = true
//        let textView = UITextView(frame: CGRect(x: 0, y: view.center.y,
//                                                width: UIScreen.main.bounds.width, height: 30))
//        textView.textAlignment = .center
//        textView.font = UIFont(name: "Helvetica", size: 30)
//        textView.textColor = drawColor
//        textView.layer.shadowColor = UIColor.black.cgColor
//        textView.layer.shadowOffset = CGSize(width: 1.0, height: 0.0)
//        textView.layer.shadowOpacity = 0.2
//        textView.layer.shadowRadius = 1.0
//        textView.layer.backgroundColor = UIColor.clear.cgColor
//        textView.autocorrectionType = .no
//        textView.isScrollEnabled = false
//        textView.delegate = self
//        self.canvasImageView.addSubview(textView)
//        self.canvasImageView.isUserInteractionEnabled = true
//        self.addGestures(view: textView)
//        self.addDoneButtonOnKeyboard(view: textView)
//        textView.becomeFirstResponder()
//    }
//
//    @IBAction func onSaveButton(_ sender: UIButton) {
//
//    }
//
//    @IBAction func onColorSelectorButton(_ sender: UIButton) {
//        self.colorLIstContainerView.isHidden = false
//        self.deleteView.isHidden = true
//        self.buttonType = .colorSelector
//        self.toolsView.isHidden = true
//    }
//
//    // MARK: - Delegate & Data Source
//    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
//        if isDrawing {
//            if let touch = touches.first {
//                lastPoint = touch.location(in: self.canvasImageView)
//            }
//        }
//    }
//
//    override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?){
//        if isDrawing {
//            if let touch = touches.first {
//                let currentPoint = touch.location(in: canvasImageView)
//                drawLineFrom(lastPoint, toPoint: currentPoint)
//                lastPoint = currentPoint
//            }
//        }
//    }
//
//    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?){
//        if isDrawing {
//            drawLineFrom(lastPoint, toPoint: lastPoint)
//        }
//
//    }
//
//    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
//        let scaleAffineTransform = CGAffineTransform.identity.scaledBy(x: scale, y: scale)
//        scrollView.contentSize = self.imageContainerView.bounds.size.applying(scaleAffineTransform)
//    }
//
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        return imageContainerView
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return colorList.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorListCollectionViewCell", for: indexPath) as! ColorListCollectionViewCell
//        cell.backgroundColor = colorList[indexPath.item]
//        cell.layer.cornerRadius = 20
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return .init(width: 40, height: 40)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let cell = collectionView.cellForItem(at: indexPath)
//        cell?.layer.borderColor = UIColor.white.cgColor
//        cell?.layer.borderWidth = 2
//        switch buttonType {
//        case .draw:
//            self.colorLIstContainerView.isHidden = false
//        default:
//            self.colorLIstContainerView.isHidden = true
//            self.toolsView.isHidden = false
//        }
//
//        self.colorSelectorButton.backgroundColor = colorList[indexPath.item]
//        self.drawColor = colorList[indexPath.item]
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//        let cell = collectionView.cellForItem(at: indexPath)
//        cell?.layer.borderColor = UIColor.clear.cgColor
//        cell?.layer.borderWidth = 0
//    }
//
//}
//
//// MARK: - Extensions
////public extension UIImage {
////
////    func suitableSize(heightLimit: CGFloat? = nil,
////                      widthLimit: CGFloat? = nil )-> CGSize? {
////
////        if let height = heightLimit {
////
////            let width = (height / self.size.height) * self.size.width
////
////            return CGSize(width: width, height: height)
////        }
////
////        if let width = widthLimit {
////            let height = (width / self.size.width) * self.size.height
////            return CGSize(width: width, height: height)
////        }
////
////        return nil
////    }
////}
