//
//  ImageAnnotationViewController.swift
//  iOSPhotoEditor
//
//  Created by Aashish Bogati on 11/06/2021.
//  Copyright © 2021 Ashish Bogati. All rights reserved.
//

import UIKit
enum ButtonType {
    case draw
    case colorSelector
}

class ImageAnnotationViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    // MARK: - Properties
    public var image: UIImage?
    var drawColor: UIColor = UIColor.red
    var isDrawing: Bool = false
    var isErasing: Bool = false
    var lastPoint: CGPoint!
    
    var lastPanPoint: CGPoint?
    var isToolBarButtonSelected: Bool = false
    @IBOutlet weak var slider: UISlider!
    
    var currentPoints = [CGPoint]()
    var points = [CGPoint]()
    
    
    @IBOutlet weak var canvasView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var canvasImageView: UIImageView!
    @IBOutlet weak var toolsView: UIView!
    @IBOutlet weak var drawButton: UIButton!
    @IBOutlet weak var addTextButton: UIButton!
    @IBOutlet weak var colorButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var canvasImageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var colorLIstContainerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var lastTextViewTransform: CGAffineTransform?
    var lastTextViewTransCenter: CGPoint?
    var lastTextViewFont:UIFont?
    var activeTextView: UITextView?
    var imageViewToPan: UIImageView?
    
    var pin1ViewCenter : CGPoint = .zero
    var pin2ViewCenter : CGPoint = .zero
    var buttonType: ButtonType = .draw
    
    var colorList: [UIColor] = [.red, .green, .black, .yellow, .orange, .brown]
    var isTyping: Bool = false
    
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureViews()
    }
    
    // MARK: - Functions
    private func configureViews() {
        self.setImageView(image: image!)
        
        self.colorLIstContainerView.isHidden = true
        self.scrollView.delegate = self
        self.scrollView.isScrollEnabled = true
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 3.0
        
        self.colorButton.layer.borderWidth = 2
        self.colorButton.layer.borderColor = UIColor.white.cgColor
        
        self.colorButton.layer.cornerRadius = self.colorButton.frame.height / 2
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow),
                                               name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillChangeFrame(_:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        self.deleteView.isHidden = true
        self.slider.isHidden = true
    }
    
    func setImageView(image: UIImage) {
        imageView.image = image
        let size = image.suitableSize(widthLimit: UIScreen.main.bounds.width)
        canvasImageHeight.constant = (size?.height)!
    }
    
    func configureNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        //        self.navigationController?.navigationItem.setRightBarButton(UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(onDoneButton)), animated: true)
        
    }
    
    func addDoneNavigationButton() {
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(onDoneButton))
        self.navigationItem.rightBarButtonItems = [doneButton]
    }
    
    func drawLineFrom(_ fromPoint: CGPoint, toPoint: CGPoint) {
        let canvasSize = canvasImageView.frame.integral.size
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, 0)
        if let context = UIGraphicsGetCurrentContext() {
            canvasImageView.image?.draw(in: CGRect(x: 0, y: 0, width: canvasSize.width, height: canvasSize.height))
            
            context.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
            context.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
            
            context.setLineCap( CGLineCap.round)
            context.setLineWidth(CGFloat(slider.value))
            context.setStrokeColor(drawColor.cgColor)
            context.setBlendMode(CGBlendMode.normal)
            
            context.strokePath()
            
            canvasImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        
    }
    
    func removeLineFrom( _ fromPoint: CGPoint, toPoint: CGPoint) {
        let canvasSize = canvasImageView.frame.integral.size
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, 0)
        if let context = UIGraphicsGetCurrentContext() {
            canvasImageView.image?.draw(in: CGRect(x: 0, y: 0, width: canvasSize.width, height: canvasSize.height))
            
            context.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
            context.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
            
            context.setLineCap(CGLineCap.round)
            context.setLineWidth(9.0)
            context.setStrokeColor(UIColor.green.cgColor)
            context.setBlendMode(CGBlendMode.clear)
            
            context.strokePath()
            
            canvasImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
    }
    
    func addDoneButtonOnKeyboard(view: UITextView){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        view.inputAccessoryView = doneToolbar
    }
    
    
    func addGestures(view: UIView) {
        //Gestures
        
        view.isUserInteractionEnabled = true
        let panGesture = UIPanGestureRecognizer(target: self,
                                                action: #selector(ImageAnnotationViewController.panGesture))
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 1
        panGesture.delegate = self
        view.addGestureRecognizer(panGesture)
        
        let pinchGesture = UIPinchGestureRecognizer(target: self,
                                                    action: #selector(ImageAnnotationViewController.pinchGesture))
        pinchGesture.delegate = self
        view.addGestureRecognizer(pinchGesture)
        
        let rotationGestureRecognizer = UIRotationGestureRecognizer(target: self,
                                                                    action:#selector(ImageAnnotationViewController.rotationGesture) )
        rotationGestureRecognizer.delegate = self
        view.addGestureRecognizer(rotationGestureRecognizer)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ImageAnnotationViewController.tapGesture))
        view.addGestureRecognizer(tapGesture)
        
    }
    
    // MARK: - Actions
    @objc func doneButtonAction(){
        self.view.endEditing(true)
        self.isTyping = false
    }
    
    @objc func onDoneButton(){
        self.scrollView.isUserInteractionEnabled = true
        self.colorLIstContainerView.isHidden = true
        self.isErasing = false
        self.slider.isHidden = true
        self.navigationItem.rightBarButtonItem = nil
        self.toolsView.isHidden = false
    }
    
    @IBAction func onDrawButton(_ sender: UIButton) {
        self.addDoneNavigationButton()
        self.isDrawing = true
        self.slider.isHidden = false
        self.buttonType = .draw
        self.colorLIstContainerView.isHidden = false
        self.toolsView.isHidden = true
        self.scrollView.isUserInteractionEnabled = false
    }
    
    @IBAction func onAddTextButton(_ sender: UIButton) {
        isTyping = true
        let textView = UITextView(frame: CGRect(x: 0, y: view.center.y,
                                                width: UIScreen.main.bounds.width, height: 30))
        textView.textAlignment = .center
        textView.font = UIFont(name: "Helvetica", size: 30)
        textView.textColor = drawColor
        textView.layer.shadowColor = UIColor.black.cgColor
        textView.layer.shadowOffset = CGSize(width: 1.0, height: 0.0)
        textView.layer.shadowOpacity = 0.2
        textView.layer.shadowRadius = 1.0
        textView.layer.backgroundColor = UIColor.clear.cgColor
        textView.autocorrectionType = .no
        textView.isScrollEnabled = false
        textView.delegate = self
        self.canvasImageView.addSubview(textView)
        self.canvasImageView.isUserInteractionEnabled = true
        self.addGestures(view: textView)
        self.addDoneButtonOnKeyboard(view: textView)
        textView.becomeFirstResponder()
    }
    
    @IBAction func onSaveButton(_ sender: UIButton) {
        
    }
    
    @IBAction func onColorSelectorButton(_ sender: UIButton) {
        self.colorLIstContainerView.isHidden = false
        self.deleteView.isHidden = true
        self.buttonType = .colorSelector
        self.toolsView.isHidden = true
    }
    
    
    @IBAction func onClearButton(_ sender: UIButton) {
        //clear stickers and textviews
        self.lastPoint  = .init(x: -100, y: -100)
        self.addDoneNavigationButton()
        self.isDrawing = false
        self.isErasing = true
        self.colorLIstContainerView.isHidden = false
        self.toolsView.isHidden = true
        self.scrollView.isUserInteractionEnabled = false
    }
    
    // MARK: - Delegate & Data Source
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        if isDrawing {
            if let touch = touches.first {
                lastPoint = touch.location(in: self.canvasImageView)
            }
        }
    }
    
    override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?){
        if isDrawing {
            if let touch = touches.first {
                let currentPoint = touch.location(in: canvasImageView)
                drawLineFrom(lastPoint, toPoint: currentPoint)
                lastPoint = currentPoint
            }
        }
        
        if isErasing {
            if let touch = touches.first {
                let currentPoint = touch.location(in: canvasImageView)
                removeLineFrom(lastPoint, toPoint: currentPoint)
                lastPoint = currentPoint
            }
        }
    }
    
    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?){
        if isDrawing {
            drawLineFrom(lastPoint, toPoint: lastPoint)
        }
        
        if isErasing {
            removeLineFrom(lastPoint, toPoint: lastPoint)
        }
        
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        let scaleAffineTransform = CGAffineTransform.identity.scaledBy(x: scale, y: scale)
        scrollView.contentSize = self.imageContainerView.bounds.size.applying(scaleAffineTransform)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colorList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorListCollectionViewCell", for: indexPath) as! ColorListCollectionViewCell
        cell.backgroundColor = colorList[indexPath.item]
        cell.layer.cornerRadius = 15
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 30, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.white.cgColor
        cell?.layer.borderWidth = 3
        switch buttonType {
        case .draw:
            self.colorLIstContainerView.isHidden = false
        default:
            self.colorLIstContainerView.isHidden = true
            self.toolsView.isHidden = false
        }
        
        self.colorButton.backgroundColor = colorList[indexPath.item]
        self.drawColor = colorList[indexPath.item]
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.white.cgColor
        cell?.layer.borderWidth = 2
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageContainerView
    }
    
}
