//
//  ColorListCollectionViewCell.swift
//  iOSPhotoEditor
//
//  Created by Aashish Bogati on 03/03/2021.
//  Copyright © 2021 Ashish Bogati. All rights reserved.
//

import UIKit

class ColorListCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Properties
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configureView()
    }
    
    // MARK: - Functions
    func configureView() {
        self.contentView.layer.cornerRadius = 20
    }
}
