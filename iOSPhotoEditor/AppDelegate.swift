//
//  AppDelegate.swift
//  iOSPhotoEditor
//
//  Created by Aashish Bogati on 01/03/2021.
//  Copyright © 2021 Ashish Bogati. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle


}

